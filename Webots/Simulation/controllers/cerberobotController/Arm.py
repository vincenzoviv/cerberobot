import math
import Gripper
from enum import IntEnum
from controller import Robot

# Class representing the mechanical arm of the robot
class Arm:

    # Class constants
    __ARM_NUM = 5

    # Enumeration that defines the arm's components
    class Arm(IntEnum):
        ARM1 = 0,
        ARM2 = 1,
        ARM3 = 2,
        ARM4 = 3,
        ARM5 = 4

    # Initializes the Arm class
    def __init__(self, cerberobot, armSpeed, gripperSpeed):
        # Arms related variables
        self.gripper = Gripper.Gripper(cerberobot, gripperSpeed)
        self.arms = []
        self.armsLength = [0.253, 0.155, 0.135, 0.081, 0.105]

        for i in range(self.Arm.ARM1, self.__ARM_NUM):
            self.arms.append(cerberobot.getDevice("arm" + str(i + 1)))
        
        self.arms[self.Arm.ARM3].setVelocity(armSpeed)
        self.reset()



    # Sets the position of the robotic arm
    def __setPosition(self, positions):
        for i in range(self.Arm.ARM1, self.__ARM_NUM):
            self.arms[i].setPosition(positions[i])



    # Brings the arm to the basic position
    def reset(self):
        self.__setPosition([0, 1.57, -2.635, 1.78, 0.0])



    # Sets arm position to a specific one indicated in radians
    def setRotation(self, arm, radian):
        self.arms[arm].setPosition(radian)



    # Sets grab position
    def setGrabPosition(self):
        self.__setPosition([0, 0, 0, -math.pi / 4, math.pi / 2])



    # Sets release position
    def setReleasePosition(self):
        self.__setPosition([0, -0.5, -1, -0.89, 0.0])



    # Sets dynamically arm position
    def setPositionXYZ(self, x, y, z):
        def insideBoundaries(val, min, max):
            delta = 0.005
            return (max - delta) if val >= max else (min + delta) if val <= min else val

        def saturateAcosArgument(value):
            if value > 1:
                return 1
            elif value < -1:
                return -1
            else:
                return value

        x = insideBoundaries(x, -0.25, 0.25)
        y = insideBoundaries(y, 0, 0.25)
        z = insideBoundaries(z, -0.25, 0.25)

        x1 = math.sqrt(x ** 2 + z ** 2)
        y1 = y + self.armsLength[self.Arm.ARM4] + self.armsLength[self.Arm.ARM5] - self.armsLength[self.Arm.ARM1]

        a = self.armsLength[self.Arm.ARM2]
        b = self.armsLength[self.Arm.ARM3]
        c = math.sqrt(x1 ** 2 + y1 ** 2)

        alpha = - math.asin(z / x1)
        beta = - (math.pi / 2 - math.acos(saturateAcosArgument((a ** 2 + c ** 2 - b ** 2) / (2.0 * a * c))) - math.atan(y1 / x1))
        gamma = - (math.pi - math.acos(saturateAcosArgument((a ** 2 + b ** 2 - c ** 2) / (2.0 * a * b))))
        delta = - (math.pi + (beta + gamma))
        epsilon = math.pi / 2 + alpha

        if beta < -1.13:
            beta = -1.13
        if gamma < -2.63:
            gamma = -2.63
        if delta < -1.78:
            delta = -1.78

        self.__setPosition([alpha, beta, gamma, delta, epsilon])



    # Fully closes the fingers
    def grip(self):
        self.gripper.grip()



    # Fully opens the fingers
    def release(self):
        self.gripper.release()



    # Sets the gap between the fingers of the gripper
    def setGap(self, gap):
        self.gripper.setGap(gap)