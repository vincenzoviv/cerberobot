from controller import Robot

# Class representing the base of the robot
class Base:

    # Class constants
    SPEED_MAX = 14.81
    WHEEL_NUM = 4

    # Initializes the Base class
    def __init__(self, cerberobot):
        self.SPEED = self.SPEED_MAX * 0.5
        self.wheels = []

        # Initialization of all wheels
        for i in range(0, self.WHEEL_NUM):
            self.wheels.append(cerberobot.getDevice("wheel" + str(i + 1)))



    # Sets the speed of all wheels
    def setVelocity(self, speeds):
        for i in range(0, self.WHEEL_NUM):
            self.wheels[i].setPosition(float('+inf'))
            self.wheels[i].setVelocity(speeds[i])


    
    # Sets to zero the speed of all wheels
    def reset(self):
        self.setVelocity([0, 0, 0, 0])


    
    # Moves forward the robot with standard speed
    def forwards(self):
        self.setVelocity([self.SPEED, self.SPEED, self.SPEED, self.SPEED])



    # Moves forward the robot with the speed given as parameter
    def forwardsWithSpeed(self, speed):
        self.setVelocity([speed, speed, speed, speed])



    # Moves backforwards the robot with standard speed
    def backwards(self):
        self.setVelocity([-self.SPEED, -self.SPEED, -self.SPEED, -self.SPEED])



    # Turns right the robot with standard speed
    def turnRight(self):
        self.setVelocity([-self.SPEED, self.SPEED, -self.SPEED, self.SPEED])



    # Turns right the robot with the speed given as parameter
    def turnRightWithSpeed(self, speed):
        self.setVelocity([-speed, speed, -speed, speed])



    # Turns left the robot with standard speed
    def turnLeft(self):
        self.setVelocity([self.SPEED, -self.SPEED, self.SPEED, -self.SPEED])



    # Turns left the robot with the speed given as parameter
    def turnLeftWithSpeed(self, speed):
        self.setVelocity([speed, -speed, speed, -speed])



    # Strafes left the robot with standard speed (lateral movement to the left)
    def strafeLeft(self):
        self.setVelocity([self.SPEED, -self.SPEED, -self.SPEED, self.SPEED])



    # Strafes left the robot with the speed given as parameter (lateral movement to the left)
    def strafeLeftWithSpeed(self, speed):
        self.setVelocity([speed, -speed, -speed, speed])



    # Strafes right the robot with standard speed (lateral movement to the right)
    def strafeRight(self):
        self.setVelocity([-self.SPEED, self.SPEED, self.SPEED, -self.SPEED])



    # Strafes right the robot with the speed given as parameter (lateral movement to the right)
    def strafeRightWithSpeed(self, speed):
        self.setVelocity([-speed, speed, speed, -speed])