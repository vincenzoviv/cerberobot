# File containing utility functions

import webcolors as wc
import math



# Returns b if v > b, returns v if v < a otherwise returns v
def bound(v, a, b):
    return b if v > b else a if v < a else v



# Pixel to millimeter conversion
def pixelToMillimeter(pixels):
    return 0.2645833333 * pixels



# Returns the predominant RGB component
def getPredominantColor(rgbValues):
    indexMax = rgbValues.index( max(rgbValues) )
    color = ""
    if indexMax == 0:
        color = "RED"
    elif indexMax == 1:
        color = "GREEN"
    else:
        color = "BLUE"
        
    return color



# Returns the values of a dictionary sorted by key
def getValuesSortedByKey(dictionary):
    values = []
    for sortedKey in sorted(dictionary):
        values.append(dictionary[sortedKey])

    return values