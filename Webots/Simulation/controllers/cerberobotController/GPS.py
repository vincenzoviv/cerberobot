# Class representing the GPS device
class GPS:

    # Initializes the GPS
    def __init__(self, cerberobot, timeStep):
        self.gps = cerberobot.getDevice("gps")
        self.gps.enable(timeStep)



    # Returns the values ​​of the GPS device
    def getValues(self):
        return self.gps.getValues()