# Class representing the Sharp GP2Y0A02YK0F device
class DistanceSensorFar:

    # Initializes the DistanceSensorFar class
    def __init__(self, cerberobot, name, timeStep):
        self.distanceSensorFar = cerberobot.getDevice(name)
        self.distanceSensorFar.enable(timeStep)



    # Computes the distance from the distance sensors of the robot
    def getValue(self):
        return 0.7611 * (self.distanceSensorFar.getValue() ** (-0.9313)) - 0.1252