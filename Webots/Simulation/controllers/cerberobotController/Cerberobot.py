import math, random, struct, re, sys
from enum import IntEnum
from controller import Robot
import utility
import GPS, Compass, Receiver, Camera, DistanceSensorFar, DistanceSensorNear, Arm, Base



# Constant that defines the step of the simulation
TIME_STEP = 32



# Class that represents our robot, in particular contains all the features and functions of the robot
class Cerberobot(Robot):

    # Definition of TargetPoint class
    class TargetPoint:

        # TargetPoint class constructor
        def __init__(self, x = 0.0, z = 0.0):
            self.x = x
            self.z = z


        # TargetPoint string representation
        def __str__(self):
            return "{ " + str(self.x) + ", " + str(self.z) + " }"



    # Definition of TargetPoint class
    class Waypoint:

        # Waypoint class constructor
        def __init__(self, ID, targetPoint, position):
            self.ID = ID
            self.targetPoint = targetPoint
            self.position = position



        # Waypoint string representation
        def __str__(self):
            return "{ " + str(self.ID) + ", " + str(self.targetPoint) + ", " + str(self.position) + " }"



    # Definition of ContainerLocator class
    class ContainerLocator:

        # ContainerLocator class constructor
        def __init__(self, ID, targetPoint, pathFromLeft, pathFromRight):
            self.ID = ID
            self.targetPoint = targetPoint
            self.pathFromLeft = pathFromLeft
            self.pathFromRight = pathFromRight



        # ContainerLocator string representation
        def __str__(self):
            return "{ " + str(self.ID) + ", " + str(self.targetPoint) + ", " + str(self.pathFromLeft) + ", " + str(self.pathFromRight) + " }"



    # Definition of CollectibleObject class
    class CollectibleObject:

        # CollectibleObject class constructor
        def __init__(self, obj, ID, color, model):
            self.obj = obj
            self.ID = ID
            self.color = color
            self.model = model



        # CollectibleObject string representation
        def __str__(self):
            return "{ " + str(self.ID) + ", " + str(self.color) + ", " + str(self.model) + " }"



    # Enumeration that defines the quadrants for moving the robot
    class Quadrant(IntEnum):
        QUADRANT_I = 0,
        QUADRANT_II = 1,
        QUADRANT_III = 2,
        QUADRANT_IV = 3



    # Enumeration that defines the directions useful for moving the robot
    class Direction(IntEnum):
        UP = 0,
        RIGHT = 1,
        DOWN = 2,
        LEFT = 3



    # Enumeration that defines the main steps of the robot functionality
    class State(IntEnum):
        SEARCH_FOR_OBJECTS = 0,
        COLLECT_OBJECT = 1,
        PUT_OBJECT_INTO_CONTAINER = 2



    # Class constants

    __DISTANCE_CAMERA_FROM_ROBOT_CENTER_X = 0.2825
    __DISTANCE_CAMERA_FROM_ROBOT_CENTER_Z = -0.0525
    # Number of distance sensors
    __DISTANCE_SENSOR_NUM = 8
    # Movement related variables
    __ANGLE_TOLERANCE = 0.01
    # Number of containers
    __CONTAINER_NUM = 9
    # Maximum number of grab attempts
    __MAX_GRASP_ATTEMPTS = 3



    # Cerberobot class constructor
    def __init__(self):
        super(Cerberobot, self).__init__()

        # GPS related variable
        self.gps = GPS.GPS(self, TIME_STEP)

        # Compass related variable
        self.compass = Compass.Compass(self, TIME_STEP)

        # Receiver related variable
        self.receiver = Receiver.Receiver(self, TIME_STEP)

        # Camera related variable
        self.camera = Camera.Camera(self, TIME_STEP)

        # Containers coordinates related dictionary
        self.containers = {}

        # Containers id related array
        self.containersID = []

        # Distance sensors related variables
        self.depthCameraFar = DistanceSensorFar.DistanceSensorFar(self, "depthCameraFar", TIME_STEP)
        self.depthCameraNear = DistanceSensorNear.DistanceSensorNear(self, "depthCameraNear", TIME_STEP)
        self.distanceSensors = []
        for i in range(0, self.__DISTANCE_SENSOR_NUM):
            self.distanceSensors.append(DistanceSensorFar.DistanceSensorFar(self, "distanceSensor" + str(i + 1), TIME_STEP))

        # Arm related variable
        self.arm = Arm.Arm(self, 0.5, 0.03)

        # Base related variable
        self.base = Base.Base(self)

        # Utility variables
        
        self.lastFreeDirections = [False, False, False, False]
        self.lastPreferredDirection = 0

        self.waypointsLeft = {}
        self.waypointsRight = {}
        self.containerLocator = None

        self.objectSelected = None
        self.objectsBlackList = []
        self.graspAttemptsNum = 0

        self.state = self.State.SEARCH_FOR_OBJECTS



    # Returns the current angle of the robot, obtained from the compass sensor
    def getAbsoluteAngle(self):
        # Get values of compass
        compassRawValues = self.compass.getValues()

        # Compute 2D vectors
        front2DVector = [compassRawValues[0], compassRawValues[1]]
        north2DVector = [1.0, 0.0]

        # Compute absolute angle
        return math.atan2(north2DVector[1], north2DVector[0]) - math.atan2(front2DVector[1], front2DVector[0])



    # Calculate the direction to rotate
    def calculateRotationDirection(self, alpha):
        theta = self.getAbsoluteAngle()
        if (theta < 0 and alpha < 0) or (theta > 0 and alpha > 0):
            if theta < alpha:
                return self.Direction.LEFT
            else:
                return self.Direction.RIGHT

        else:  
            if abs(theta) + abs(alpha) < (math.pi - abs(theta)) + (math.pi - abs(alpha)):
                return self.Direction.LEFT
            else:
                return self.Direction.RIGHT



    # Turns the robot by the specified angle
    def goToAngle(self, alpha):
        speed = self.base.SPEED
        direction = self.calculateRotationDirection(alpha)
        while True:
            theta = self.getAbsoluteAngle()

            if self.around(theta, alpha, self.__ANGLE_TOLERANCE):
                return

            if (theta < 0 and alpha < 0) or (theta > 0 and alpha > 0):
                if abs(theta - alpha) <= 0.1:
                    speed = 1
                
                if direction == self.Direction.LEFT:
                    # Left
                    self.base.turnLeftWithSpeed(speed)
                else:
                    # Right
                    self.base.turnRightWithSpeed(speed)

            else:
                if direction == self.Direction.LEFT:
                    # Left
                    if abs(theta) + abs(alpha) <= 0.1:
                        speed = 1
                    self.base.turnLeftWithSpeed(speed)
                else:
                    # Right
                    if (math.pi - abs(theta)) + (math.pi - abs(alpha)) <= 0.1:
                        speed = 1
                    self.base.turnRightWithSpeed(speed)

            self.__step()



    # Returns the target point having the coordinates that the robot must go to so that it can be found at offset meters away from the object
    def objectTargetPoint(self, distance, offset):
        xRobot, yRobot, zRobot = self.gps.getValues()
        theta = self.getAbsoluteAngle()

        if theta >= - math.pi and theta <= - math.pi / 2:
            angle = - (abs(theta) - math.pi / 2)
        elif theta >= - math.pi / 2 and theta <= 0:
            angle = math.pi / 2 - abs(theta)
        elif theta >= math.pi / 2 and theta <= math.pi:
            angle = - (math.pi / 2 + (math.pi / 2 - (theta - math.pi / 2)))
        else:
            angle = math.pi / 2 + theta
        
        x1 = (distance * math.sin(angle))
        x2 = (self.__DISTANCE_CAMERA_FROM_ROBOT_CENTER_X * math.sin(angle))
        x = x1 + x2 + xRobot - (self.__DISTANCE_CAMERA_FROM_ROBOT_CENTER_X + offset) * (-1 if (theta >= - math.pi and theta <= - math.pi / 2) or (theta >= math.pi / 2 and theta <= math.pi) else 1)

        z1 = (distance * math.cos(angle))
        z2 = (self.__DISTANCE_CAMERA_FROM_ROBOT_CENTER_X * math.cos(angle))
        z = z1 + z2 + zRobot

        return self.TargetPoint(x, z)



    # Goes one step forward in the simulation
    def __step(self):
        if self.step(TIME_STEP) == -1:
            exit(0)



    # Waits for the time specified given as parameter
    def passiveWait(self, sec):
        startTime = self.getTime()
        self.__step()
        while startTime + sec > self.getTime():
            self.__step()



    # Allows the arm to grab an object
    def grabObject(self, distance, objHeight):
        # Initial position for grasping
        self.arm.setGrabPosition()
        self.passiveWait(1)

        # Spread the fingers
        self.arm.release()
        self.passiveWait(1)

        # Slowly go down until you grab the object
        h_per_step = 0.005
        y = objHeight + 0.0125
        x = distance + 0.2
        h = 0.25
        while h > y:
            self.arm.setPositionXYZ(x, h, 0)
            self.__step()
            h -= h_per_step

        # Close the fingers
        self.arm.grip()
        self.passiveWait(1)

        h = y
        while h < 0.25:
            self.arm.setPositionXYZ(x, h, 0)
            self.__step()
            h += h_per_step

        # Initial position for grasping
        self.arm.setGrabPosition()
        self.passiveWait(1)



    # Allows the arm to put an object into a container
    def releaseObject(self):
        self.arm.setReleasePosition()
        self.passiveWait(3)

        self.arm.release()
        self.passiveWait(1)

        self.arm.reset()
        self.passiveWait(2)



    # Returns the object with the largest bounding box currently in the camera
    def getWidestObject(self):
        objects = self.camera.observe()

        # Take the object nearest to the robot
        positionsOnImage = []
        objectsFiltered = []

        # I run through all the objects and select the largest ones
        for object in objects:
            if object.get_size_on_image()[0] >= self.camera.WIDTH * 0.05:
                positionsOnImage.append(object.get_size_on_image()[0])
                objectsFiltered.append(object)

        if len(objectsFiltered) == 0:
            return None

        return objectsFiltered[ positionsOnImage.index( max(positionsOnImage) ) ]



    # Returns one of the objects that can be grabbed
    def chooseValidObject(self):
        widestObject = self.getWidestObject()

        if widestObject == None or widestObject.get_id() in self.containersID or widestObject.get_id() in self.objectsBlackList:
            # Do not select objects that have an id that is either that of the containers or that of the objects that at the moment you have not been able to grasp
            return None
        else:
            return widestObject



    # Allows the robot to move freely in the environment (avoiding obstacles) and to select one of the valid objects
    def searchForObjects(self, distanceTolerance):
        while True:
            objectSelected = self.chooseValidObject()
            if objectSelected != None:
                self.objectSelected = self.CollectibleObject(objectSelected, objectSelected.get_id(), utility.getPredominantColor(objectSelected.get_colors()), objectSelected.get_model().decode("utf-8"))
                self.state = self.State.COLLECT_OBJECT
                return
            self.avoidObstacles(distanceTolerance)
            self.__step()

    

    # Performs full process of locating and storing containers
    def storeContainersPosition(self):
        while len(self.containers) < self.__CONTAINER_NUM:
            container = self.getWidestObject()
            offset = 0.15

            id = container.get_id()
            color = utility.getPredominantColor(container.get_colors())
            model = container.get_model()

            while True:
                container = self.getRefreshedRecognizedObject(container)
                self.centerObjectPosition(container)
                self.base.forwards()

                if self.depthCameraNear.getValue() <= offset:
                    break

                self.__step()
            
            self.base.reset()
            self.passiveWait(0.25)
            self.goToAngle(math.pi)
            
            self.containerStorage(id, color, model)

            self.base.backwards()
            self.passiveWait(1)

            x, y, z = self.gps.getValues()
            if len(self.containers) < self.__CONTAINER_NUM:
                self.goToXZ(self.TargetPoint(x, z - 0.5), 0.05, math.pi)
                self.goToAngle(math.pi)
            else:
                self.base.turnLeft()
                self.passiveWait(4)
                self.base.reset()
                self.passiveWait(0.25)



    # Centers the object in the robot camera
    def centerObjectPosition(self, object):
        while True:
            if self.getObjectByID(object.get_id()) == None:
                return False
            
            position = self.getRefreshedRecognizedObject(object).get_position_on_image()[0]
            distanceNear, distanceFar = self.depthCameraNear.getValue(), self.depthCameraFar.getValue()
            distance = distanceNear if distanceNear <= 0.39 else distanceFar

            # Computes tolerance and speed as a function of distance from the object
            pixelTolerance = math.ceil((int(round(self.camera.WIDTH_CENTER * 0.01)) / distance))
            strafeSpeed = min([1 / distance, 1])

            if position > self.camera.WIDTH_CENTER + pixelTolerance:
                if self.getDistance(self.Direction.RIGHT) <= 0.2:
                    if self.distanceSensors[1].getValue() <= 0.35:
                        self.base.strafeLeftWithSpeed(strafeSpeed)
                    else:
                        self.base.turnRight()
                        self.__step()
                        self.base.strafeLeftWithSpeed(strafeSpeed)
                else:
                    # Strafes right to center the object
                    self.base.strafeRightWithSpeed(strafeSpeed)

            elif position < self.camera.WIDTH_CENTER - pixelTolerance:
                if self.getDistance(self.Direction.LEFT) <= 0.2:
                    if self.distanceSensors[0].getValue() <= 0.35:
                        self.base.strafeRightWithSpeed(strafeSpeed)
                    else:
                        self.base.turnLeft()
                        self.__step()
                        self.base.strafeRightWithSpeed(strafeSpeed)
                else:
                    # Strafes left to center the object
                    self.base.strafeLeftWithSpeed(strafeSpeed)
            else:
                # The object is centered
                return True
            
            self.__step()



    # Returns an object with updated values whenever possible
    def getRefreshedRecognizedObject(self, object):
        for obj in self.camera.observe():
            if obj.get_id() == object.get_id():
                return obj
        return object



    # Corrects the angle of the robot and centers the object to be picked up
    def adjustAngle(self):
        obj =  self.objectSelected.obj
        objectInitialAngle = obj.get_orientation()[3]
        tolerance = math.pi / 16
        if self.objectSelected.model == "cylinder" or self.around(objectInitialAngle, 0, tolerance) or self.around(objectInitialAngle, math.pi / 2, tolerance) or self.around(objectInitialAngle, math.pi, tolerance):
            return

        direction = self.Direction.LEFT if self.distanceSensors[0].getValue() > self.distanceSensors[1].getValue() else self.Direction.RIGHT
        tolerance /= 2
        while True:
            obj = self.getRefreshedRecognizedObject(obj)
            angleCurrent = obj.get_orientation()[3]

            if self.around(angleCurrent, 0, tolerance) or self.around(angleCurrent, math.pi / 2, tolerance) or self.around(angleCurrent, math.pi, tolerance):
                return

            if direction == self.Direction.LEFT:
                self.base.turnLeft()
            else:
                self.base.turnRight()
            self.__step()
            self.centerObjectPosition(obj)



    # Performs the physical process of grasping the object
    def collectObject(self):
        obj = self.objectSelected.obj

        offset = 0.125
        speed = self.base.SPEED
        isObjectStillInFrame = True
        while True:
            obj = self.getRefreshedRecognizedObject(obj)
        
            isObjectStillInFrame = self.centerObjectPosition(obj)

            distanceFar, distanceNear = self.depthCameraFar.getValue(), self.depthCameraNear.getValue()

            # Calculate the height of the object
            if (distanceNear < 0.35):
                objectHeight = self.camera.getObjectHeight(obj, distanceNear, distanceFar)

            if not isObjectStillInFrame:
                # Object disappeared from camera view
                self.state = self.State.SEARCH_FOR_OBJECTS
                return

            if distanceNear <= offset * 1.5:
                self.adjustAngle()

            elif distanceNear <= offset * 2:
                speed = self.base.SPEED / 16
            
            self.base.forwardsWithSpeed(speed)

            if distanceNear <= offset:
                break

            self.__step()
        
        self.base.reset()
        self.passiveWait(0.25)
        self.grabObject(distanceNear, objectHeight)

        if self.getObjectByID(self.objectSelected.ID) == None:
            # The object disappeared from the camera's view, so it was most likely grabbed
            self.graspAttemptsNum = 0
            self.objectsBlackList = []
            self.state = self.State.PUT_OBJECT_INTO_CONTAINER
        else:
            # The mechanical arm was unable to retrieve the object
            self.graspAttemptsNum += 1
            if self.graspAttemptsNum == self.__MAX_GRASP_ATTEMPTS:
                # If the robot tried to grab the object __MAX_GRASP_ATTEMPTS times without success, then enter the object id in the black list
                self.objectsBlackList.append(self.objectSelected.ID)
                self.graspAttemptsNum = 0

                # Strafes left or right until the object disappears from the camera view
                self.strafeUntilObjectDisappears(self.objectSelected.ID)

                # Search for new objects
                self.state = self.State.SEARCH_FOR_OBJECTS

            else:
                # Try to grab the same object again
                self.state = self.State.COLLECT_OBJECT
            
            # Reset the position of the mechanical arm
            self.arm.reset()
            self.passiveWait(2)



    # Strafes left or right until the object disappears from the camera view
    def strafeUntilObjectDisappears(self, objectID):
        while True:
            object = self.getObjectByID(objectID)

            if object == None:
                # The object has disappeared from the camera view
                return

            position = object.get_position_on_image()[0]

            if position < self.camera.WIDTH_CENTER:
                # The object is to the left of the robot
                self.base.strafeRight()
            else:
                # The object is to the right of the robot
                self.base.strafeLeft()

            self.__step()



    # Retrieve the object having the id passed as a parameter
    def getObjectByID(self, objectID):
        for obj in self.camera.observe():
            if obj.get_id() == objectID:
                return obj



    # Moves the robot to the container corresponding to the collected object and places it inside of the container
    def putObjectIntoContainer(self, distanceTolerance):
        # Localizes the corresponding container of the current object
        self.goToContainer()

        containerTargetPoint = self.localizeContainer()
        containerTargetPointNear = self.TargetPoint(containerTargetPoint.x + (distanceTolerance + 0.1), containerTargetPoint.z)

        # Moves the robot near the container
        self.goToXZ(containerTargetPointNear, distanceTolerance, math.pi)

        while True:
            self.base.forwardsWithSpeed(self.base.SPEED / 4)
            if self.depthCameraNear.getValue() <= 0.15:
                break
            self.__step()

        self.base.reset()
        self.passiveWait(0.25)

        self.releaseObject()

        # Choose whether to turn right or left at random and at a random angle that can range from [-pi/2, 0] (left) or from [0, pi/2] (right)
        if random.uniform(0, 1) >= random.uniform(0, 1):
            # Left
            angle = - math.pi / 2 + math.pi / 2 * random.uniform(0, 1)
        else:
            # Right
            angle = math.pi / 2 - math.pi / 2 * random.uniform(0, 1)
        self.goToAngle(angle)

        self.base.reset()
        self.passiveWait(0.25)

        self.state = self.State.SEARCH_FOR_OBJECTS



    # Allows the robot to recognize the containers
    def containerStorage(self, id, color, model):
        coordsTmp = self.gps.getValues()
        self.containers[color + "-" + model.decode("utf-8")] = [coordsTmp[0], coordsTmp[2]]
        self.containersID.append(id)



    # Sets the coordinate of corresponding model and color container
    def localizeContainer(self):
        key = self.objectSelected.color + "-" + self.objectSelected.model
        if key in self.containers:
            return self.TargetPoint(self.containers[key][0], self.containers[key][1])



    # Returns a list containing all the waypoints needed to get to the containers
    def getPathToContainers(self):
        zRobot = self.gps.getValues()[2]

        if zRobot >= self.containerLocator.targetPoint.z:
            # Left
            if len(self.waypointsLeft) == 0:
                return [self.containerLocator]
            
            pathFromLeft = utility.getValuesSortedByKey(self.waypointsLeft)
            pathFromLeft.append(self.containerLocator)

            for i in range(0, len(pathFromLeft) - 1):
                if zRobot <= pathFromLeft[-2 - i].targetPoint.z and zRobot >= pathFromLeft[-1 - i].targetPoint.z:
                    result = pathFromLeft[-1 - i : -1]
                    result.append(pathFromLeft[-1])
                    return result
            return pathFromLeft

        else:
            # Right
            if len(self.waypointsRight) == 0:
                return [self.containerLocator]
            
            pathFromRight = utility.getValuesSortedByKey(self.waypointsRight)
            pathFromRight.append(self.containerLocator)

            for i in range(0, len(pathFromRight) - 1):
                if zRobot >= pathFromRight[-2 - i].targetPoint.z and zRobot <= pathFromRight[-1 - i].targetPoint.z:
                    result = pathFromRight[-1 - i : -1]
                    result.append(pathFromRight[-1])
                    return result
            return pathFromRight



    # Moves the robot to the master locator
    def goToMasterLocator(self):
        path = self.getPathToContainers()
        masterLocator = path.pop()

        for node in path:
            self.goToXZ(node.targetPoint, 0.5)

        self.goToXZ(masterLocator.targetPoint, 0.5, math.pi)



    # Moves the robot to the last waypoint before the containers
    def goToContainer(self):
        path = self.getPathToContainers()
        # The route to the container is managed separately
        path.pop()

        for node in path:
            self.goToXZ(node.targetPoint, 0.5)



    # Initializes locator objects
    def buildLocatorObjects(self, message):
        message = message.decode("utf-8")

        locatorID = re.search('ID: (.*)\n', message).group(1)
        locatorTargetPoint = self.TargetPoint(float(re.search('X: (.*)\n', message).group(1)) + 1, float(re.search('Z: (.*)\n', message).group(1)))

        locatorType = re.search('TYPE: (.*)\n', message).group(1)

        if locatorType == "SLAVE":
            position = re.search('POSITION: (.*)\n', message).group(1)
            waypoint = self.Waypoint(locatorID, locatorTargetPoint, position)

            # Adds to list if not present
            if position == "LEFT" and not locatorID in self.waypointsLeft:
                self.waypointsLeft[locatorID] = waypoint

            elif position == "RIGHT" and not locatorID in self.waypointsRight:
                self.waypointsRight[locatorID] = waypoint

        elif locatorType == "MASTER" and self.containerLocator == None:
            # There can only be one master
            pathFromLeft = re.search('PATH_FROM_LEFT: (.*)\n', message).group(1)
            pathFromLeft = ([] if pathFromLeft == "[]" else pathFromLeft.strip('][').split(', '))
            pathFromRight = re.search('PATH_FROM_RIGHT: (.*)\n', message).group(1)
            pathFromRight = ([] if pathFromRight == "[]" else pathFromRight.strip('][').split(', '))

            locatorTargetPoint.x += 0.5

            self.containerLocator = self.ContainerLocator(locatorID, locatorTargetPoint, pathFromLeft, pathFromRight)



    # Returns the coordinates of the emitter
    def receiveMessageFromLocator(self):
        queueLength = self.receiver.getQueueLength()
        while queueLength > 0:
            data = self.receiver.getData()
            (i,), data = struct.unpack("I", data[:4]), data[4:]
            s, data = data[:i], data[i:]
            self.buildLocatorObjects(s)
            self.receiver.nextPacket()
            queueLength -= 1



    # Receives all packets with locator informations
    def receiveAllMessagesFromLocators(self):
        while True:
            self.receiveMessageFromLocator()
            self.__step()
            if self.containerLocator != None and len(self.containerLocator.pathFromLeft) == len(self.waypointsLeft) and len(self.containerLocator.pathFromRight) == len(self.waypointsRight):
                self.receiver.disable()
                return



    # Returns the free directions and the array of distances
    def freeDirections(self, distanceTolerance):
        freeDirections = [False, False, False, False]
        freeDirections[0] = self.getDistance(self.Direction.UP) >= distanceTolerance
        freeDirections[1] = self.getDistance(self.Direction.RIGHT) >= distanceTolerance
        freeDirections[2] = self.getDistance(self.Direction.DOWN) >= distanceTolerance
        freeDirections[3] = self.getDistance(self.Direction.LEFT) >= distanceTolerance

        return freeDirections



    # It decides whether to prefer the direction to the right or to the left
    def preferredDirection(self):
        rightDistance = self.getDistance(self.Direction.RIGHT)
        leftDistance = self.getDistance(self.Direction.LEFT)

        return (self.Direction.RIGHT if rightDistance >= leftDistance else self.Direction.LEFT)



    # Moves the robot avoiding obstacles
    def avoidObstacles(self, distanceTolerance):
        freeDirections = self.freeDirections(distanceTolerance)

        # Four free directions
        if freeDirections == [True, True, True, True]:
            self.base.forwards()

        # Three free directions
        elif freeDirections == [False, True, True, True]:
            # If it has already experienced this situation in the previous step,
            # then chooses the previously chosen direction of rotation again
            if self.lastFreeDirections == [False, True, True, True]:
                arbitraryDirection = self.lastPreferredDirection
            else:
                # Computes the preferred direction and update the variable that stores it
                self.lastPreferredDirection = arbitraryDirection = self.preferredDirection()

            if arbitraryDirection == self.Direction.RIGHT:
                self.base.turnRight()
            else:
                self.base.turnLeft()

        elif freeDirections == [True, False, True, True]:
            # Allows to perform unnecessary left rotations
            if self.getDistance(self.Direction.RIGHT) <= 0.2:
                self.base.turnLeft()
                self.__step()
                self.base.forwards()
            else:
                self.base.forwards()

        elif freeDirections == [True, True, True, False]:
            # Allows you to perform unnecessary right rotations
            if self.getDistance(self.Direction.LEFT) <= 0.2:
                self.base.turnRight()
                self.__step()
                self.base.forwards()
            else:
                self.base.forwards()

        elif freeDirections == [True, True, False, True]:
            self.base.forwards()

        # Two free directions
        elif freeDirections == [False, False, True, True]:
            self.base.turnLeft()

        elif freeDirections == [False, True, False, True]:
            # If it has already experienced this situation in the previous step,
            # then chooses the previously chosen direction of strafe again
            if self.getDistance(self.Direction.UP) <= distanceTolerance / 2:
                if self.lastFreeDirections == [False, True, False, True]:
                    arbitraryDirection = self.lastPreferredDirection
                else:
                    # Computes the preferred direction and update the variable that stores it
                    self.lastPreferredDirection = arbitraryDirection = self.preferredDirection()

                if arbitraryDirection == self.Direction.RIGHT:
                    self.base.strafeRight()
                else:
                    self.base.strafeLeft()
            else:
                self.base.forwards()

        elif freeDirections == [True, False, False, True]:
            self.base.turnLeft()

        elif freeDirections == [False, True, True, False]:
            self.base.turnRight()

        elif freeDirections == [True, False, True, False]:
            self.base.forwards()

        elif freeDirections == [True, True, False, False]:
            self.base.turnRight()

        # One free direction
        elif freeDirections == [True, False, False, False]:
            self.base.forwards()

        elif freeDirections == [False, True, False, False]:
            self.base.strafeRight()

        elif freeDirections == [False, False, True, False]:
            self.base.backwards()

        elif freeDirections == [False, False, False, True]:
            self.base.strafeLeft()

        # Updates the last direction
        self.lastFreeDirections = freeDirections
        self.__step()



    # Returns the quadrant in which the target point passed as parameter relative to the robot reference system is found
    def getQuadrant(self, targetPoint, angle):
        gpsRawValues = self.gps.getValues()
        x = gpsRawValues[0]
        z = gpsRawValues[2]
        if targetPoint.x < x and targetPoint.z < z:
            if angle == 0:
                return self.Quadrant.QUADRANT_III
            else:
                return self.Quadrant.QUADRANT_I
        elif targetPoint.x > x and targetPoint.z < z:
            if angle == 0:
                return self.Quadrant.QUADRANT_IV
            else:
                return self.Quadrant.QUADRANT_II
        elif targetPoint.x > x and targetPoint.z > z:
            if angle == 0:
                return self.Quadrant.QUADRANT_I
            else:
                return self.Quadrant.QUADRANT_III
        elif targetPoint.x < x and targetPoint.z > z:
            if angle == 0:
                return self.Quadrant.QUADRANT_II
            else:
                return self.Quadrant.QUADRANT_IV



    # Check if the direction is really free, not being influenced by the oscillations of the sensor values
    def isBypassObstacleFreeDirection(self, direction, distanceTolerance):
        offset = 0.015
        if direction == self.Direction.UP or direction == self.Direction.DOWN:
            return self.getDistance(direction) >= distanceTolerance + offset
        else:
            return self.getDistance(direction) >= 0.25 + offset



    # Allows the robot to overtake the obstacle 
    def bypassObstacle(self, distanceTolerance, moveDirection, checkDirection, angle):
        freeDirections = [False, False, False, False]
        freeDirections[moveDirection] = True

        self.base.reset()
        self.passiveWait(0.2)

        while freeDirections[moveDirection] == True and freeDirections[checkDirection] == False:
            self.executeBypassObstacleFunction(moveDirection)
            self.__step()
            freeDirections = self.freeDirectionsXZ(distanceTolerance)
            freeDirections[checkDirection] = self.isBypassObstacleFreeDirection(checkDirection, distanceTolerance)

        if freeDirections[moveDirection] == True:
            MIN_ITERATION = 8 if moveDirection == self.Direction.UP or moveDirection == self.Direction.DOWN else 4
            MAX_ITERATION = MIN_ITERATION * 2

            i = 0
            while i < MAX_ITERATION:
                if i > MIN_ITERATION and freeDirections[moveDirection] == False:
                    break
                self.executeBypassObstacleFunction(moveDirection)
                self.__step()
                i += 1
                freeDirections = self.freeDirectionsXZ(distanceTolerance)

        self.base.reset()
        self.passiveWait(0.2)
        self.straighten(angle, 0.1)

        oppositeDirection = self.getOppositeDirection(moveDirection)
        MIN_ITERATION = 30
        i = 0
        while freeDirections[checkDirection] == True:
            if i > MIN_ITERATION and freeDirections[oppositeDirection] == True:
                break
            self.executeBypassObstacleFunction(checkDirection)
            self.__step()
            i += 1
            freeDirections = self.freeDirectionsXZ(distanceTolerance)
            freeDirections[oppositeDirection] = self.isBypassObstacleFreeDirection(oppositeDirection, distanceTolerance)

        if freeDirections[checkDirection] == True:
            MIN_ITERATION = 8 if moveDirection == self.Direction.UP or moveDirection == self.Direction.DOWN else 4
            MAX_ITERATION = MIN_ITERATION * 2

            i = 0
            while i < MAX_ITERATION:
                if i > MIN_ITERATION and freeDirections[checkDirection] == False:
                    break
                self.executeBypassObstacleFunction(checkDirection)
                self.__step()
                i += 1
                freeDirections = self.freeDirectionsXZ(distanceTolerance)
        
        self.base.reset()
        self.passiveWait(0.2)
        self.straighten(angle, 0.1)



    # Performs the motion function used to overtake the obstacle
    def executeBypassObstacleFunction(self, moveDirection):
        if moveDirection == self.Direction.UP:
            self.base.forwards()

        elif moveDirection == self.Direction.RIGHT:
            self.base.strafeRight()

        elif moveDirection == self.Direction.DOWN:
            self.base.backwards()

        else:
            self.base.strafeLeft()



    # Check if first coordinate is around second coordinate (based on tolerance)
    def around(self, coord1, coord2, tolerance):
        return (coord1 >= coord2 - tolerance and coord1 <= coord2 + tolerance)



    # Returns the opposite direction of the robot
    def getOppositeDirection(self, direction):
        if direction == self.Direction.UP:
            return self.Direction.DOWN
        elif direction == self.Direction.RIGHT:
            return self.Direction.LEFT
        elif direction == self.Direction.DOWN:
            return self.Direction.UP
        else:
            return self.Direction.RIGHT



    # Checks if the robot is x aligned with respect to the target point
    def alignedOnX(self, quadrant):
        if quadrant == self.Quadrant.QUADRANT_I or quadrant == self.Quadrant.QUADRANT_II:
            self.base.strafeRight()
        else:
            self.base.strafeLeft()



    # Checks if the robot is z aligned with respect to the target point
    def alignedOnZ(self, quadrant):
        if quadrant == self.Quadrant.QUADRANT_I or quadrant == self.Quadrant.QUADRANT_IV:
            self.base.forwards()
        else:
            self.base.backwards()



    # Moves the robot to the target point without avoiding obstacles
    def goToXZNoObstacle(self, xRobot, zRobot, targetPoint, quadrant, tolerance):
        if self.around(xRobot, targetPoint.x, tolerance):
            self.alignedOnX(quadrant)

        elif self.around(zRobot, targetPoint.z, tolerance):
            self.alignedOnZ(quadrant)

        else:
            if abs(xRobot - targetPoint.x) >= abs(zRobot - targetPoint.z):
                if quadrant == self.Quadrant.QUADRANT_I or quadrant == self.Quadrant.QUADRANT_IV:
                    self.base.forwards()
                else:
                    self.base.backwards()

            else:
                if quadrant == self.Quadrant.QUADRANT_I or quadrant == self.Quadrant.QUADRANT_II:
                    self.base.strafeRight()
                else:
                    self.base.strafeLeft()



    # Checks if the quadrant matches the direction given as a parameter
    def isQuadrantDirection(self, quadrant, direction):
        if direction == self.Direction.UP:
            return (quadrant == self.Quadrant.QUADRANT_I or quadrant == self.Quadrant.QUADRANT_IV)
        elif direction == self.Direction.RIGHT:
            return (quadrant == self.Quadrant.QUADRANT_I or quadrant == self.Quadrant.QUADRANT_II)
        elif direction == self.Direction.DOWN:
            return (quadrant == self.Quadrant.QUADRANT_II or quadrant == self.Quadrant.QUADRANT_III)
        elif direction == self.Direction.LEFT:
            return (quadrant == self.Quadrant.QUADRANT_III or quadrant == self.Quadrant.QUADRANT_IV)



    # Returns the minimum distance, detected by the distance sensor, in the direction given as a parameter
    def getDistance(self, direction):
        if direction == self.Direction.UP:
            return min([self.distanceSensors[0].getValue(), self.depthCameraFar.getValue(), self.distanceSensors[1].getValue()])
        elif direction == self.Direction.RIGHT:
            return min([self.distanceSensors[2].getValue(), self.distanceSensors[3].getValue()])
        elif direction == self.Direction.DOWN:
            return min([self.distanceSensors[4].getValue(), self.distanceSensors[5].getValue()])
        elif direction == self.Direction.LEFT:
            return min([self.distanceSensors[6].getValue(), self.distanceSensors[7].getValue()])



    # Straightens the robot based on tolerance
    def straighten(self, angle, angleTolerance):
        # Always keep the angle around pi
        if not self.around(self.getAbsoluteAngle(), angle, angleTolerance) and not self.around(self.getAbsoluteAngle(), angle, angleTolerance):
            self.goToAngle(angle)



    # Returns the free directions and the array of distances
    def freeDirectionsXZ(self, distanceTolerance):
        freeDirections = [False, False, False, False]
        freeDirections[0] = self.getDistance(self.Direction.UP) >= distanceTolerance
        freeDirections[1] = self.getDistance(self.Direction.RIGHT) >= 0.25
        freeDirections[2] = self.getDistance(self.Direction.DOWN) >= distanceTolerance
        freeDirections[3] = self.getDistance(self.Direction.LEFT) >= 0.25

        return freeDirections



    # Moves the robot to the target point avoiding obstacles
    def goToXZ(self, targetPoint, distanceTolerance, angle = None):
        if angle == None:
            # Automatically determines the angle
            angle = 0 if self.gps.getValues()[0] < targetPoint.x else math.pi

        if angle != 0 and angle != math.pi and angle != - math.pi:
            print("The angle must be zero, pi or minus pi", file = sys.stderr)
            return

        tolerance = 0.05
        startTime = self.getTime()
        self.straighten(angle, 0.01)
        while True:

            if self.getTime() >= startTime + 5:
                self.straighten(angle, 0.05)
                startTime = self.getTime()

            self.straighten(angle, 0.1)

            gpsRawValues = self.gps.getValues()
            xRobot = gpsRawValues[0]
            zRobot = gpsRawValues[2]
            
            if self.around(xRobot, targetPoint.x, tolerance) and self.around(zRobot, targetPoint.z, tolerance):
                self.base.reset()
                self.passiveWait(0.25)
                return

            freeDirections = self.freeDirectionsXZ(distanceTolerance)
            quadrant = self.getQuadrant(targetPoint, angle)

            # Four free directions
            if freeDirections == [True, True, True, True]:
                self.goToXZNoObstacle(xRobot, zRobot, targetPoint, quadrant, tolerance)

            # Three free directions
            elif freeDirections == [False, True, True, True]:
                if self.around(xRobot, targetPoint.x, tolerance):
                    self.alignedOnX(quadrant)
                if self.around(zRobot, targetPoint.z, tolerance) and self.isQuadrantDirection(quadrant, self.Direction.DOWN):
                    self.alignedOnZ(quadrant)
                elif quadrant == self.Quadrant.QUADRANT_I:
                    self.bypassObstacle(distanceTolerance, self.Direction.RIGHT, self.Direction.UP, angle)
                elif quadrant == self.Quadrant.QUADRANT_IV:
                    self.bypassObstacle(distanceTolerance, self.Direction.LEFT, self.Direction.UP, angle)
                else:
                    self.goToXZNoObstacle(xRobot, zRobot, targetPoint, quadrant, tolerance)

            elif freeDirections == [True, False, True, True]:
                if self.around(zRobot, targetPoint.z, tolerance):
                    self.alignedOnZ(quadrant)
                elif self.around(xRobot, targetPoint.x, tolerance) and self.isQuadrantDirection(quadrant, self.Direction.LEFT):
                    self.alignedOnX(quadrant)
                elif quadrant == self.Quadrant.QUADRANT_I:
                    self.bypassObstacle(distanceTolerance, self.Direction.UP, self.Direction.RIGHT, angle)
                elif quadrant == self.Quadrant.QUADRANT_II:
                    self.bypassObstacle(distanceTolerance, self.Direction.DOWN, self.Direction.RIGHT, angle)
                else:
                    self.goToXZNoObstacle(xRobot, zRobot, targetPoint, quadrant, tolerance)

            elif freeDirections == [True, True, True, False]:
                if self.around(zRobot, targetPoint.z, tolerance):
                    self.alignedOnZ(quadrant)
                elif self.around(xRobot, targetPoint.x, tolerance) and self.isQuadrantDirection(quadrant, self.Direction.RIGHT):
                    self.alignedOnX(quadrant)
                elif quadrant == self.Quadrant.QUADRANT_III:
                    self.bypassObstacle(distanceTolerance, self.Direction.DOWN, self.Direction.LEFT, angle)
                elif quadrant == self.Quadrant.QUADRANT_IV:
                    self.bypassObstacle(distanceTolerance, self.Direction.UP, self.Direction.LEFT, angle)
                else:
                    self.goToXZNoObstacle(xRobot, zRobot, targetPoint, quadrant, tolerance)

            elif freeDirections == [True, True, False, True]:
                if self.around(xRobot, targetPoint.x, tolerance):
                    self.alignedOnX(quadrant)
                if self.around(zRobot, targetPoint.z, tolerance) and self.isQuadrantDirection(quadrant, self.Direction.UP):
                    self.alignedOnZ(quadrant)
                elif quadrant == self.Quadrant.QUADRANT_II:
                    self.bypassObstacle(distanceTolerance, self.Direction.RIGHT, self.Direction.DOWN, angle)
                elif quadrant == self.Quadrant.QUADRANT_III:
                    self.bypassObstacle(distanceTolerance, self.Direction.LEFT, self.Direction.DOWN, angle)
                else:
                    self.goToXZNoObstacle(xRobot, zRobot, targetPoint, quadrant, tolerance)

            # Two free directions
            elif freeDirections == [False, False, True, True]:
                if self.around(xRobot, targetPoint.x, tolerance) and self.isQuadrantDirection(quadrant, self.Direction.LEFT):
                    self.alignedOnX(quadrant)
                elif self.around(zRobot, targetPoint.z, tolerance) and self.isQuadrantDirection(quadrant, self.Direction.DOWN):
                    self.alignedOnZ(quadrant)
                elif quadrant == self.Quadrant.QUADRANT_I:
                    self.bypassObstacle(distanceTolerance, self.Direction.LEFT, self.Direction.UP, angle)
                elif quadrant == self.Quadrant.QUADRANT_II:
                    self.bypassObstacle(distanceTolerance, self.Direction.DOWN, self.Direction.RIGHT, angle)
                elif quadrant == self.Quadrant.QUADRANT_IV:
                    self.bypassObstacle(distanceTolerance, self.Direction.LEFT, self.Direction.UP, angle)
                else:
                    self.goToXZNoObstacle(xRobot, zRobot, targetPoint, quadrant, tolerance)

            elif freeDirections == [False, True, False, True]:
                if self.around(xRobot, targetPoint.x, tolerance):
                    self.alignedOnX(quadrant)
                elif quadrant == self.Quadrant.QUADRANT_I:
                    self.bypassObstacle(distanceTolerance, self.Direction.RIGHT, self.Direction.UP, angle)
                elif quadrant == self.Quadrant.QUADRANT_II:
                    self.bypassObstacle(distanceTolerance, self.Direction.RIGHT, self.Direction.DOWN, angle)
                elif quadrant == self.Quadrant.QUADRANT_III:
                    self.bypassObstacle(distanceTolerance, self.Direction.LEFT, self.Direction.DOWN, angle)
                else:
                    self.bypassObstacle(distanceTolerance, self.Direction.LEFT, self.Direction.UP, angle)

            elif freeDirections == [True, False, False, True]:
                if self.around(xRobot, targetPoint.x, tolerance) and self.isQuadrantDirection(quadrant, self.Direction.LEFT):
                    self.alignedOnX(quadrant)
                elif self.around(zRobot, targetPoint.z, tolerance) and self.isQuadrantDirection(quadrant, self.Direction.UP):
                    self.alignedOnZ(quadrant)
                elif quadrant == self.Quadrant.QUADRANT_I:
                    self.bypassObstacle(distanceTolerance, self.Direction.UP, self.Direction.RIGHT, angle)
                elif quadrant == self.Quadrant.QUADRANT_II:
                    self.bypassObstacle(distanceTolerance, self.Direction.UP, self.Direction.RIGHT, angle)
                elif quadrant == self.Quadrant.QUADRANT_III:
                    self.bypassObstacle(distanceTolerance, self.Direction.LEFT, self.Direction.DOWN, angle)
                else:
                    self.goToXZNoObstacle(xRobot, zRobot, targetPoint, quadrant, tolerance)

            elif freeDirections == [False, True, True, False]:
                if self.around(xRobot, targetPoint.x, tolerance) and self.isQuadrantDirection(quadrant, self.Direction.RIGHT):
                    self.alignedOnX(quadrant)
                elif self.around(zRobot, targetPoint.z, tolerance) and self.isQuadrantDirection(quadrant, self.Direction.DOWN):
                    self.alignedOnZ(quadrant)
                if quadrant == self.Quadrant.QUADRANT_I:
                    self.bypassObstacle(distanceTolerance, self.Direction.RIGHT, self.Direction.UP, angle)
                elif quadrant == self.Quadrant.QUADRANT_III:
                    self.bypassObstacle(distanceTolerance, self.Direction.DOWN, self.Direction.LEFT, angle)
                elif quadrant == self.Quadrant.QUADRANT_IV:
                    self.bypassObstacle(distanceTolerance, self.Direction.DOWN, self.Direction.LEFT, angle)
                else:
                    self.goToXZNoObstacle(xRobot, zRobot, targetPoint, quadrant, tolerance)

            elif freeDirections == [True, False, True, False]:
                if self.around(zRobot, targetPoint.z, tolerance):
                    self.alignedOnZ(quadrant)
                elif quadrant == self.Quadrant.QUADRANT_I:
                    self.bypassObstacle(distanceTolerance, self.Direction.UP, self.Direction.RIGHT, angle)
                elif quadrant == self.Quadrant.QUADRANT_II:
                    self.bypassObstacle(distanceTolerance, self.Direction.DOWN, self.Direction.RIGHT, angle)
                elif quadrant == self.Quadrant.QUADRANT_III:
                    self.bypassObstacle(distanceTolerance, self.Direction.DOWN, self.Direction.LEFT, angle)
                else:
                    self.bypassObstacle(distanceTolerance, self.Direction.UP, self.Direction.LEFT, angle)

            elif freeDirections == [True, True, False, False]:
                if self.around(xRobot, targetPoint.x, tolerance) and self.isQuadrantDirection(quadrant, self.Direction.RIGHT):
                    self.alignedOnX(quadrant)
                elif self.around(zRobot, targetPoint.z, tolerance) and self.isQuadrantDirection(quadrant, self.Direction.UP):
                    self.alignedOnZ(quadrant)
                elif quadrant == self.Quadrant.QUADRANT_II:
                    self.bypassObstacle(distanceTolerance, self.Direction.RIGHT, self.Direction.DOWN, angle)
                elif quadrant == self.Quadrant.QUADRANT_III:
                    self.bypassObstacle(distanceTolerance, self.Direction.RIGHT, self.Direction.DOWN, angle)
                elif quadrant == self.Quadrant.QUADRANT_IV:
                    self.bypassObstacle(distanceTolerance, self.Direction.UP, self.Direction.LEFT, angle)
                else:
                    self.goToXZNoObstacle(xRobot, zRobot, targetPoint, quadrant, tolerance)

            # One free direction
            elif freeDirections == [True, False, False, False]:
                if self.around(zRobot, targetPoint.z, tolerance) and self.isQuadrantDirection(quadrant, self.Direction.UP):
                    self.alignedOnZ(quadrant)
                elif quadrant == self.Quadrant.QUADRANT_I:
                    self.bypassObstacle(distanceTolerance, self.Direction.UP, self.Direction.RIGHT, angle)
                elif quadrant == self.Quadrant.QUADRANT_II:
                    self.bypassObstacle(distanceTolerance, self.Direction.UP, self.Direction.RIGHT, angle)
                elif quadrant == self.Quadrant.QUADRANT_III:
                    self.bypassObstacle(distanceTolerance, self.Direction.UP, self.Direction.LEFT, angle)
                else:
                    self.bypassObstacle(distanceTolerance, self.Direction.UP, self.Direction.LEFT, angle)

            elif freeDirections == [False, True, False, False]:
                if self.around(xRobot, targetPoint.x, tolerance) and self.isQuadrantDirection(quadrant, self.Direction.RIGHT):
                    self.alignedOnX(quadrant)
                elif quadrant == self.Quadrant.QUADRANT_I:
                    self.bypassObstacle(distanceTolerance, self.Direction.RIGHT, self.Direction.UP, angle)
                elif quadrant == self.Quadrant.QUADRANT_II:
                    self.bypassObstacle(distanceTolerance, self.Direction.RIGHT, self.Direction.DOWN, angle)
                elif quadrant == self.Quadrant.QUADRANT_III:
                    self.bypassObstacle(distanceTolerance, self.Direction.RIGHT, self.Direction.DOWN, angle)
                else:
                    self.bypassObstacle(distanceTolerance, self.Direction.RIGHT, self.Direction.UP, angle)

            elif freeDirections == [False, False, True, False]:
                if self.around(zRobot, targetPoint.z, tolerance) and self.isQuadrantDirection(quadrant, self.Direction.DOWN):
                    self.alignedOnZ(quadrant)
                elif quadrant == self.Quadrant.QUADRANT_I:
                    self.bypassObstacle(distanceTolerance, self.Direction.DOWN, self.Direction.RIGHT, angle)
                elif quadrant == self.Quadrant.QUADRANT_II:
                    self.bypassObstacle(distanceTolerance, self.Direction.DOWN, self.Direction.RIGHT, angle)
                elif quadrant == self.Quadrant.QUADRANT_III:
                    self.bypassObstacle(distanceTolerance, self.Direction.DOWN, self.Direction.LEFT, angle)
                else:
                    self.bypassObstacle(distanceTolerance, self.Direction.DOWN, self.Direction.LEFT, angle)

            elif freeDirections == [False, False, False, True]:
                if self.around(xRobot, targetPoint.x, tolerance) and self.isQuadrantDirection(quadrant, self.Direction.LEFT):
                    self.alignedOnX(quadrant)
                elif quadrant == self.Quadrant.QUADRANT_I:
                    self.bypassObstacle(distanceTolerance, self.Direction.LEFT, self.Direction.UP, angle)
                elif quadrant == self.Quadrant.QUADRANT_II:
                    self.bypassObstacle(distanceTolerance, self.Direction.LEFT, self.Direction.DOWN, angle)
                elif quadrant == self.Quadrant.QUADRANT_III:
                    self.bypassObstacle(distanceTolerance, self.Direction.LEFT, self.Direction.DOWN, angle)
                else:
                    self.bypassObstacle(distanceTolerance, self.Direction.LEFT, self.Direction.UP, angle)

            self.__step()



    # Main function of the robot
    def run(self):
        self.receiveAllMessagesFromLocators()
        self.goToMasterLocator()
        self.storeContainersPosition()

        while self.step(TIME_STEP) != -1:
            if self.state == self.State.SEARCH_FOR_OBJECTS:
                self.searchForObjects(0.5)
            elif self.state == self.State.COLLECT_OBJECT:
                self.collectObject()
            elif self.state == self.State.PUT_OBJECT_INTO_CONTAINER:
                self.putObjectIntoContainer(0.5)