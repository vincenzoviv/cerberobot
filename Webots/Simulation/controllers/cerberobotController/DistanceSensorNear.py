# Class representing the Sharp GP2Y0A41SK0F device
class DistanceSensorNear:

    # Initializes the DistanceSensorNear class
    def __init__(self, cerberobot, name, timeStep):
        self.distanceSensorNear = cerberobot.getDevice(name)
        self.distanceSensorNear.enable(timeStep)



    # Computes the distance from the distance sensors of the robot
    def getValue(self):
        return 0.1594 * (self.distanceSensorNear.getValue() ** (-0.8533)) - 0.02916