import utility
from enum import IntEnum

# Class representing the robot gripper
class Gripper:

    # Class constants
    __MIN_POS = 0.0
    __MAX_POS = 0.025
    __OFFSET_WHEN_LOCKED = 0.021

    # Initializes the gripper
    def __init__(self, cerberobot, speed):
        self.leftFinger = cerberobot.getDevice("finger1")
        self.leftFinger.setVelocity(speed)

        self.rightFinger = cerberobot.getDevice("finger2")
        self.rightFinger.setVelocity(speed)



    # Sets the position of the fingers
    def __setPosition(self, leftPosition, rightPosition):
        self.leftFinger.setPosition(leftPosition)
        self.rightFinger.setPosition(rightPosition)



    # Fully closes the fingers
    def grip(self):
        self.__setPosition(self.__MIN_POS, self.__MIN_POS)



    # Fully opens the fingers
    def release(self):
        self.__setPosition(self.__MAX_POS, self.__MAX_POS)



    # Sets the gap between the fingers of the gripper
    def setGap(self, gap):
        v = utility.bound(0.5 * (gap - self.__OFFSET_WHEN_LOCKED), self.__MIN_POS, self.__MAX_POS)
        self.__setPosition(v, v)