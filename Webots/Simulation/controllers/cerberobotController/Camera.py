import utility

# Class representing the camera device
class Camera:
    
    # Initializes the Camera class
    def __init__(self, cerberobot, timeStep):
        self.camera = cerberobot.getDevice("camera")
        self.camera.enable(timeStep)
        self.camera.recognitionEnable(timeStep)

        self.WIDTH = self.camera.getWidth()
        self.WIDTH_CENTER = int(round(self.camera.getWidth() / 2))



    # Enables the recognition object mode
    def observe(self):
        return self.camera.getRecognitionObjects()



    # Returns the number of objects in the focus of the camera
    def numObjects(self):
        return self.camera.getRecognitionNumberOfObjects()



    # Computes the height of an object from the camera image
    def getObjectHeight(self, obj, distanceNear, distanceFar):
        distance = distanceNear if distanceNear <= 0.4 else distanceFar

        focalLength = self.camera.getFocalLength()
        objHeightOnImage = obj.get_size_on_image()[1]
        sensorHeight = self.camera.getHeight()
        sensorHeightInMillimeters = utility.pixelToMillimeter(sensorHeight)

        return ((distance / focalLength) * (objHeightOnImage / sensorHeight) * sensorHeightInMillimeters) / 100