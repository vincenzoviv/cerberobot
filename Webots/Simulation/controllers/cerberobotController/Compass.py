# Class representing the compass device
class Compass:

    # Initializes the Compass class
    def __init__(self, cerberobot, timeStep):
        self.compass = cerberobot.getDevice("compass")
        self.compass.enable(timeStep)



    # Returns the values ​​of the compass device
    def getValues(self):
        return self.compass.getValues()