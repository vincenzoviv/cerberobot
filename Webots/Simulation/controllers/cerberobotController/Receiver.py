# Class representing the receiver device
class Receiver:

    # Initializes the receiver
    def __init__(self, cerberobot, timeStep):
        self.receiver = cerberobot.getDevice("receiver")
        self.receiver.enable(timeStep)



    # Returns the data obtained from the receiver
    def getData(self):
        return self.receiver.getData()



    # Disables the receiver
    def disable(self):
        self.receiver.disable()



    # Gets the reference to the next received packet
    def nextPacket(self):
        self.receiver.nextPacket()



    # Return the length of the packet queue
    def getQueueLength(self):
        return self.receiver.getQueueLength()