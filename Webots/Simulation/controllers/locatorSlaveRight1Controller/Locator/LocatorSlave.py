import sys, struct
from Locator.Locator import Locator

# Class LocatorSlave
class LocatorSlave(Locator):

	# Initializes the slave locator
	def __init__(self, timeStep, id, position):
		super(LocatorSlave, self).__init__(timeStep)
		self.id = id
		self.position = position



	# Constructs the message to be sent
	def buildMessage(self):
		self.step(self.timeStep)
		coords = self.gps.getValues()

		locatorType = "TYPE: " + "SLAVE"
		locatorID = "ID: " + str(self.id)
		locatorCoordinateX = "X: " + str(coords[0])
		locatorCoordinateZ = "Z: " + str(coords[2])
		position = "POSITION: " + str(self.position)

		return locatorType + "\n" + locatorID + "\n" + locatorCoordinateX + "\n" + locatorCoordinateZ + "\n" + position + "\n"