import sys, struct
from controller import Robot



# Class Locator
class Locator(Robot):

	# Initializes the locator
	def __init__(self, timeStep):
		super(Locator, self).__init__()
		self.timeStep = timeStep
		self.gps = None
		self.emitter = None
		self.gpsInit()
		self.emitterInit()



	# GPS sensor init
	def gpsInit(self):
		self.gps = self.getDevice("gps")

		if self.gps:
			self.gps.enable(self.timeStep)
		else:
			print("Cannot use GPS", file = sys.stderr)



	# Emitter sensor init
	def emitterInit(self):
		self.emitter = self.getDevice("emitter")
		if not self.emitter:
			print("Cannot use emitter", file = sys.stderr)



	# A function that must necessarily be overridden by the subclasses of this class
	def buildMessage(self):
		pass



	# Returns the coordinates of the emitter
	def sendMessage(self):
		s = bytes(self.buildMessage(), 'utf-8')
		data = struct.pack("I%ds" % (len(s),), len(s), s)
		self.emitter.send(data)



	# Send a message at each specified interval
	def sendMessageWithInterval(self, sec):
		self.sendMessage()
		startTime = self.getTime()
		self.step(self.timeStep)
		while self.step(self.timeStep) != -1:
			if self.getTime() >= startTime + sec:
				self.sendMessage()
				startTime = self.getTime()