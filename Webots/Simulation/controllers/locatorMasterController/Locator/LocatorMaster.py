import sys, struct
from Locator.Locator import Locator

# Class LocatorMaster
class LocatorMaster(Locator):

	# Initializes the master locator
	def __init__(self, timeStep, id, pathFromLeft, pathFromRight):
		super(LocatorMaster, self).__init__(timeStep)
		self.id = id
		self.pathFromLeft = pathFromLeft
		self.pathFromRight = pathFromRight



	# Constructs the message to be sent
	def buildMessage(self):
		self.step(self.timeStep)
		coords = self.gps.getValues()

		locatorType = "TYPE: " + "MASTER"
		locatorID = "ID: " + str(self.id)
		locatorCoordinateX = "X: " + str(coords[0])
		locatorCoordinateZ = "Z: " + str(coords[2])
		locatorPathFromLeft = "PATH_FROM_LEFT: " + self.pathFromLeft
		locatorPathFromRight = "PATH_FROM_RIGHT: " + self.pathFromRight

		return locatorType + "\n" + locatorID + "\n" + locatorCoordinateX + "\n" + locatorCoordinateZ + "\n" + locatorPathFromLeft + "\n" + locatorPathFromRight + "\n"